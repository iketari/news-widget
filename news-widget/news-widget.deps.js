({
	mustDeps: [
		{
			block: 'news-widget-promo'
		},
		{
			block: 'news-widget-tiding'
		},
		{
			block: 'news-widget-transport'
		},
		{
			block: 'b-spinner'
		},
		{
			block: 'b-link'
		}
	]
});
