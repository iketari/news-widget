/// <amd-module name="news-widget/news-widget/news-widget"/>
/// <amd-dependency path="toolkit/b-toolkit/b-toolkit" name="toolkit"/>
/// <amd-dependency path="Promise" name="Promise"/>

import {Transport} from 'news-widget/news-widget-transport/news-widget-transport';

const COUNTER_HOLDER_SELECTOR ='.js-button[data-name="ph-more"]';
const ANIMATION_DELAY = 1500;

declare var toolkit: any;
declare var Promise: any;

interface Promo {
	date: string;
	action?: string;
	href?: string;
	img: string;
	text: string;
	title: string;
	closed?: boolean;
	seen?: boolean;
}

interface Tiding {
	date: string;
	action?: string;
	href?: string;
	img: string;
	text: string;
	title: string;
	seen?: boolean;
}

interface PromoData {
	promo: Promo[],
	tidings: Tiding[]
}

let NewsWidget = toolkit.create('news-widget', {

	/**
	 * Инициализация виджета "Новости Почты"
	 */
	init () {
		this.transport = new Transport(this.getData().transport);

		this.onPromoClose = this.onPromoClose.bind(this);
		this.onAction = this.onAction.bind(this);

		this._initEvents();
	},

	/**
	 * Развешиваем события
	 */
	_initEvents () {
		this.$el.on('tap.' + this.uniqId, '.js-close', this.onPromoClose);
		this.$el.on('tap.' + this.uniqId, '[data-id]', this.onAction);
	},

	/**
	 * Тап по крестику промо-блока
	 * @param {Event} event
	 */
	onPromoClose (event: Event) {
		event.stopPropagation(); //TODO: С этим нужно что-то сделать

		this.setLastRejectPromoTS();
		this.$('.news-widget-promo').hide();
		this.fixate('promo:close');
	},

	/**
	 * Там по [data-id] элементам, используется для логирования.
	 * @param {Event} event
	 */
	onAction (event: Event) {
		let element = <HTMLElement>event.currentTarget,
			action = element.getAttribute('data-id'),
			name = element.getAttribute('data-name');

		if (name === 'tiding_link') {
			action = action + ':link';
		}

		this.fixate(action);
	},

	/**
	 * Отрисовка элемента
	 */
	render () {
		let needFetch = !this.getData().news;
		let promise = Promise.resolve();

		if (needFetch) {
			promise = this.fetchData();
		}

		promise.always(() => {
			toolkit['b-toolkit'].prototype.render.apply(this);
			this.renderCounter();
		}).fail((err) => {
			this.fixate('error', err);
		});

		return this;
	},

	/**
	 * Отрисовка элмента счетчика
	 */
	renderCounter () {
		let $counter = this._getCounter(),
			newsCount = this._getUnread();

		$counter.html(newsCount);
		$counter.toggle(!!newsCount);
		this.fixate(`counter:value:${newsCount}`);

		this.$counterHolder.append($counter);
	},

	/**
	 * Загрузка данных
	 * @returns {Promise}
	 */
	fetchData () {
		return this.transport.get(this.getData().url)
			.then(this.prepareData.bind(this))
			.then(data => {
				this.extendData({
					news: data
				});

				this.fixate('fetch', data);

				return data;
			})
			.fail(err => {
				Promise.reject(err);
			});
	},

	/**
	 * Подготовка данных для отрисовки
	 * @param  {PromoData}    data
	 * @return {PromoData}
	 */
	prepareData (data:PromoData): PromoData {
		let maxPromos = this.getData().maxPromos,
			maxTidings = this.getData().maxTidings,
			sortTidings = this.getData().sortTidings;

		data.promo = data.promo || [];
		data.tidings = data.tidings || [];

		if (maxPromos) {
			data.promo = data.promo.slice(0, maxPromos);
		}

		if (sortTidings) {
			data.tidings = data.tidings.sort(sortTidings);
		}

		if (maxTidings) {
			data.tidings = data.tidings.slice(0, maxTidings);
		}

		this._marksBanners(data);

		return data;
	},

	/**
	 * Помечаем баннеры просмотренными
	 * @param {PromoData} data
	 */
	_marksBanners (data: PromoData) {
		data.promo.forEach((promo, index) => {
			let toSeen = +promo.date < this.getLastSeenTS();
			let toClose = +promo.date < this.getLastRejectPromoTS();

			if (!promo.action) {
				promo.action = `promo:${index}`;
			}

			promo.closed = toClose;
			promo.seen = toSeen;
		});

		data.tidings.forEach((tiding, index) => {
			let toSeen = +tiding.date < this.getLastSeenTS();

			if (!tiding.action) {
				tiding.action = `tiding:${index}`;
			}

			tiding.seen = toSeen;
		});
	},

	/**
	 * Число непрочитанных новостей
	 * @return {number}
	 */
	_getUnread (): number {
		let tidings = (this.data.news && this.data.news.tidings) || [];
		let promo = (this.data.news && this.data.news.promo) || [];

		let items = tidings.concat(promo);
		return items.reduce((sum, tiding) => {
			return tiding.seen ? sum : sum + 1;
		}, 0);
	},

	/**
	 * После того, как потух каунтер, обновляем данные о последнем просмотре промо
	 */
	_bindOutside () {
		setTimeout(() => {
			this.updateLastSeen();
		}, ANIMATION_DELAY);
	},

	/**
	 * Обновляем время последнего просмотра Новостей
	 */
	updateLastSeen () {
		this.setLastSeenTS().then(this.refresh.bind(this), () => {
			this.fixate('error', ['setLastSeenTS']);
		});
	},

	/**
	 * Показываем виджет и ждет закрытия
	 */
	open () {
		this._bindOutside();

		this.$('.news-widget')
			.add(this._getCounter())
			.toggleMod('toseen', true);
	},

	/**
	 * Обновляем интерфейс
	 */
	refresh () {
		this.extendData({
			news: null
		});

		this.render();
		this.fixate('widget:refresh');
	},

	/**
	 * Запоминаем время последнего открытия виджета
	 * @override
	 */
	setLastSeenTS () {},

	/**
	 * Запоминаем время последнего закрытия promo блока
	 * @override
	 */
	setLastRejectPromoTS () {},

	/**
	 * Время последнего открытия виджета
	 * @override
	 * @returns {number}
	 */
	getLastSeenTS (): number {
		return 0;
	},

	/**
	 * Время последнего закрытия promo блока
	 * @returns {number} timestamp
	 */
	getLastRejectPromoTS (): number {
		return 0;
	},

	/**
	 * Кэшируем DOM узел, в который положим каунтер
	 */
	_setCounterHolder () {
		if (this.counterHolderSelector) {
			this.$counterHolder = this.$(this.counterHolderSelector);
		} else {
			this.$counterHolder = $(COUNTER_HOLDER_SELECTOR);
		}
	},

	/**
	 * Получения DOM объекта холдера для счетчика
	 * @private
	 * @returns {JQuery}
	 */
	_getCounter (): JQuery {
		this._setCounterHolder();

		if (!this._$counter) {
			this._$counter = $('<div class="news-widget__counter"/>');
		}

		return this._$counter;
	},

	/**
	 * Функция для фиксации данных в статистике
	 * @override
	 * @param {string} action
	 */
	externalFixate (action: string) {
		console.log('ui-abstract-action', {chain: ['news-widget'].concat(action)});
	},

	/**
	 * Отсылаем статистические данные
	 * @param {string} action
	 * @param {any} data
	 */
	fixate (action: string, data: any) {
		this.emit(action, data);
		this.externalFixate(action);
	}
});

export {NewsWidget};
