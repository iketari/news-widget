/// <amd-module name="news-widget/news-widget/news-widget"/>
/// <amd-dependency path="toolkit/b-toolkit/b-toolkit" name="toolkit"/>
/// <amd-dependency path="Promise" name="Promise"/>
define("news-widget/news-widget/news-widget", ["require", "exports", "toolkit/b-toolkit/b-toolkit", "Promise", 'news-widget/news-widget-transport/news-widget-transport'], function (require, exports, toolkit, Promise, news_widget_transport_1) {
    "use strict";
    var COUNTER_HOLDER_SELECTOR = '.js-button[data-name="ph-more"]';
    var ANIMATION_DELAY = 1500;
    var NewsWidget = toolkit.create('news-widget', {
        /**
         * Инициализация виджета "Новости Почты"
         */
        init: function () {
            this.transport = new news_widget_transport_1.Transport(this.getData().transport);
            this.onPromoClose = this.onPromoClose.bind(this);
            this.onAction = this.onAction.bind(this);
            this._initEvents();
        },
        /**
         * Развешиваем события
         */
        _initEvents: function () {
            this.$el.on('tap.' + this.uniqId, '.js-close', this.onPromoClose);
            this.$el.on('tap.' + this.uniqId, '[data-id]', this.onAction);
        },
        /**
         * Тап по крестику промо-блока
         * @param {Event} event
         */
        onPromoClose: function (event) {
            event.stopPropagation(); //TODO: С этим нужно что-то сделать
            this.setLastRejectPromoTS();
            this.$('.news-widget-promo').hide();
            this.fixate('promo:close');
        },
        /**
         * Там по [data-id] элементам, используется для логирования.
         * @param {Event} event
         */
        onAction: function (event) {
            var element = event.currentTarget, action = element.getAttribute('data-id'), name = element.getAttribute('data-name');
            if (name === 'tiding_link') {
                action = action + ':link';
            }
            this.fixate(action);
        },
        /**
         * Отрисовка элемента
         */
        render: function () {
            var _this = this;
            var needFetch = !this.getData().news;
            var promise = Promise.resolve();
            if (needFetch) {
                promise = this.fetchData();
            }
            promise.always(function () {
                toolkit['b-toolkit'].prototype.render.apply(_this);
                _this.renderCounter();
            }).fail(function (err) {
                _this.fixate('error', err);
            });
            return this;
        },
        /**
         * Отрисовка элмента счетчика
         */
        renderCounter: function () {
            var $counter = this._getCounter(), newsCount = this._getUnread();
            $counter.html(newsCount);
            $counter.toggle(!!newsCount);
            this.fixate("counter:value:" + newsCount);
            this.$counterHolder.append($counter);
        },
        /**
         * Загрузка данных
         * @returns {Promise}
         */
        fetchData: function () {
            var _this = this;
            return this.transport.get(this.getData().url)
                .then(this.prepareData.bind(this))
                .then(function (data) {
                _this.extendData({
                    news: data
                });
                _this.fixate('fetch', data);
                return data;
            })
                .fail(function (err) {
                Promise.reject(err);
            });
        },
        /**
         * Подготовка данных для отрисовки
         * @param  {PromoData}    data
         * @return {PromoData}
         */
        prepareData: function (data) {
            var maxPromos = this.getData().maxPromos, maxTidings = this.getData().maxTidings, sortTidings = this.getData().sortTidings;
            data.promo = data.promo || [];
            data.tidings = data.tidings || [];
            if (maxPromos) {
                data.promo = data.promo.slice(0, maxPromos);
            }
            if (sortTidings) {
                data.tidings = data.tidings.sort(sortTidings);
            }
            if (maxTidings) {
                data.tidings = data.tidings.slice(0, maxTidings);
            }
            this._marksBanners(data);
            return data;
        },
        /**
         * Помечаем баннеры просмотренными
         * @param {PromoData} data
         */
        _marksBanners: function (data) {
            var _this = this;
            data.promo.forEach(function (promo, index) {
                var toSeen = +promo.date < _this.getLastSeenTS();
                var toClose = +promo.date < _this.getLastRejectPromoTS();
                if (!promo.action) {
                    promo.action = "promo:" + index;
                }
                promo.closed = toClose;
                promo.seen = toSeen;
            });
            data.tidings.forEach(function (tiding, index) {
                var toSeen = +tiding.date < _this.getLastSeenTS();
                if (!tiding.action) {
                    tiding.action = "tiding:" + index;
                }
                tiding.seen = toSeen;
            });
        },
        /**
         * Число непрочитанных новостей
         * @return {number}
         */
        _getUnread: function () {
            var tidings = (this.data.news && this.data.news.tidings) || [];
            var promo = (this.data.news && this.data.news.promo) || [];
            var items = tidings.concat(promo);
            return items.reduce(function (sum, tiding) {
                return tiding.seen ? sum : sum + 1;
            }, 0);
        },
        /**
         * После того, как потух каунтер, обновляем данные о последнем просмотре промо
         */
        _bindOutside: function () {
            var _this = this;
            setTimeout(function () {
                _this.updateLastSeen();
            }, ANIMATION_DELAY);
        },
        /**
         * Обновляем время последнего просмотра Новостей
         */
        updateLastSeen: function () {
            var _this = this;
            this.setLastSeenTS().then(this.refresh.bind(this), function () {
                _this.fixate('error', ['setLastSeenTS']);
            });
        },
        /**
         * Показываем виджет и ждет закрытия
         */
        open: function () {
            this._bindOutside();
            this.$('.news-widget')
                .add(this._getCounter())
                .toggleMod('toseen', true);
        },
        /**
         * Обновляем интерфейс
         */
        refresh: function () {
            this.extendData({
                news: null
            });
            this.render();
            this.fixate('widget:refresh');
        },
        /**
         * Запоминаем время последнего открытия виджета
         * @override
         */
        setLastSeenTS: function () { },
        /**
         * Запоминаем время последнего закрытия promo блока
         * @override
         */
        setLastRejectPromoTS: function () { },
        /**
         * Время последнего открытия виджета
         * @override
         * @returns {number}
         */
        getLastSeenTS: function () {
            return 0;
        },
        /**
         * Время последнего закрытия promo блока
         * @returns {number} timestamp
         */
        getLastRejectPromoTS: function () {
            return 0;
        },
        /**
         * Кэшируем DOM узел, в который положим каунтер
         */
        _setCounterHolder: function () {
            if (this.counterHolderSelector) {
                this.$counterHolder = this.$(this.counterHolderSelector);
            }
            else {
                this.$counterHolder = $(COUNTER_HOLDER_SELECTOR);
            }
        },
        /**
         * Получения DOM объекта холдера для счетчика
         * @private
         * @returns {JQuery}
         */
        _getCounter: function () {
            this._setCounterHolder();
            if (!this._$counter) {
                this._$counter = $('<div class="news-widget__counter"/>');
            }
            return this._$counter;
        },
        /**
         * Функция для фиксации данных в статистике
         * @override
         * @param {string} action
         */
        externalFixate: function (action) {
            console.log('ui-abstract-action', { chain: ['news-widget'].concat(action) });
        },
        /**
         * Отсылаем статистические данные
         * @param {string} action
         * @param {any} data
         */
        fixate: function (action, data) {
            this.emit(action, data);
            this.externalFixate(action);
        }
    });
    exports.NewsWidget = NewsWidget;
});
//# sourceMappingURL=news-widget.js.map