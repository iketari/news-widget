var RPC = require('RPC');

module.exports = {
	init: function (Component, data) {
		data.data.dateToString = function (timestamp) {
			var result = '',
				date = new Date(+timestamp),
				months = [
					'янв',
					'фев',
					'мар',
					'апр',
					'мая',
					'июн',
					'июл',
					'авг',
					'сен',
					'окт',
					'ноя',
					'дек'
				];

			if (date.getDate()) {
				result = date.getDate() + ' ' + months[date.getMonth()];
			}

			return result;
		};

		data.data.sortTidings = function (a, b) {
			return new Date(+b.date) - new Date(+a.date);
		};

		return new Component(data);
	}
};