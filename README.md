# Пакет реализует виджет "Новости почты"
#### Подробнее можно прочитать в  *[MAIL-XXX](https://jira.mail.ru/browse/XXX)*

#### Посмотреть на работу пакета в изолированном окружении можно [здесь](XXX)

### Разработка

#### Пакет реализован с использованием [TypeScript](https://www.typescriptlang.org/)

#### Для начала разработки необходимо установить:

- nodejs >= 6.0.0
- npm >= 3.0.0
- grunt

#### и выполнить:

```
npm install
grunt shell:watch
```

#### Чтобы собрать TypeScript:

```
grunt shell:build
```

#### Чтобы поднять патч версию и опубликовать пакет

```
grunt publish
```