/* jshint node: true */
'use strict';

var pkgJson = require('./package.json');

module.exports = function (grunt) {
	grunt.config.init({
		'jam-publisher': {
			options: {
				repo: 'http://grunt-publisher:grunt-publisher@dragonballs.dev.mail.ru:5984/jam-packages', // jshint ignore:line
				level: 'error',
				cfg: pkgJson
			}
		},

		bump: {
			options: {
				files: ['package.json'],
				updateConfigs: [],
				commit: true,
				commitMessage: '%VERSION%',
				commitFiles: ['package.json'],
				createTag: true,
				tagName: '%VERSION%',
				push: true,
				pushTo: 'origin',
				gitDescribeOptions: '--tags --always --abbrev=1 --dirty=-d',
				globalReplace: false,
				prereleaseName: false,
				regExp: false
			}
		},

		shell: {
			build: {
				command: './node_modules/typescript/bin/tsc'
			},

			watch: {
				command: './node_modules/typescript/bin/tsc --watch'
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-bump');
	grunt.loadNpmTasks('grunt-jam-publisher');
	grunt.loadNpmTasks('grunt-shell');

	grunt.registerTask('publish', [
		'shell:build', 'bump', 'jam-publisher'
	]);
};