var RPC = require('RPC');

module.exports = {
	init: function (Component, data) {
		data.data.dateToString = function (timestamp) {
			var date = new Date(timestamp),
				months = [
					'янв',
					'фев',
					'мар',
					'апр',
					'мая',
					'июн',
					'июл',
					'авг',
					'сен',
					'окт',
					'ноя',
					'дек'
				];

			return date.getDay() + ' ' + months[date.getMonth()];
		};

		return new Component(data);
	}
};