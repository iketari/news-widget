///<amd-module name="news-widget/news-widget-transport/news-widget-transport"/>
/// <amd-dependency path="Promise" name="Promise"/>
/// <amd-dependency path="RPC" name="RPC"/>
/// <amd-dependency path="toolkit/b-toolkit/b-toolkit" name="toolkit"/>
define("news-widget/news-widget-transport/news-widget-transport", ["require", "exports", "Promise", "RPC", "toolkit/b-toolkit/b-toolkit", 'news-widget/news-widget-transport/news-widget-transport.mocks'], function (require, exports, Promise, RPC, toolkit, news_widget_transport_mocks_1) {
    "use strict";
    var providers = {
        mock: {
            get: function () {
                return new Promise(function (resolve) {
                    resolve(news_widget_transport_mocks_1.default);
                });
            }
        },
        cors: {
            get: function (url) {
                return new Promise(function (resolve, reject) {
                    $.ajax({
                        url: url,
                        dataType: 'json',
                        crossDomain: true
                    }).done(resolve).fail(reject);
                });
            }
        }
    };
    var Transport = (function () {
        function Transport(type) {
            if (type === void 0) { type = 'cors'; }
            this.type = type;
            this.provider = providers[type];
        }
        Transport.prototype.get = function (url) {
            return this.provider.get(url);
        };
        return Transport;
    }());
    exports.Transport = Transport;
});
//# sourceMappingURL=news-widget-transport.js.map