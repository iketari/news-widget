///<amd-module name="news-widget/news-widget-transport/news-widget-transport"/>
/// <amd-dependency path="Promise" name="Promise"/>
/// <amd-dependency path="RPC" name="RPC"/>
/// <amd-dependency path="toolkit/b-toolkit/b-toolkit" name="toolkit"/>

import mockData from 'news-widget/news-widget-transport/news-widget-transport.mocks';

interface Provider {
	get(url: string): Promise<Object>;
}

interface TransportInterface {
	type: string;
	provider: Provider;
	get(url?: string): Promise<Object>;
}

let providers = {

	mock: <Provider>{
		get: function () {
			return new Promise(function (resolve) {
				resolve(mockData);
			});
		}
	},

	cors: <Provider>{
		get: function (url) {
			return new Promise(function (resolve, reject) {
				$.ajax({
					url: url,
					dataType: 'json',
					crossDomain: true
				}).done(resolve).fail(reject);
			});
		}
	}
};

class Transport implements TransportInterface {
	type: string;
	provider: Provider;

	constructor (type = 'cors') {
		this.type = type;
		this.provider = providers[type];
	}

	get (url) {
		return this.provider.get(url);
	}
}

export {Transport, TransportInterface};
