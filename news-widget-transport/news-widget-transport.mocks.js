///<amd-module name="news-widget/news-widget-transport/news-widget-transport.mocks"/>
define("news-widget/news-widget-transport/news-widget-transport.mocks", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.default = {
        "promo": [
            {
                "img": "https://cdn4.iconfinder.com/data/icons/longico/224/longico-23-512.png",
                "text": "Вступайте в ряды бета-тестеров Почты!",
                "href": null,
                "title": "Участвовать",
                "action": "beta-tester",
                "date": "1467299722336"
            }
        ],
        "tidings": [
            {
                "img": "https://filin.mail.ru/pic?email=a.mezin@corp.mail.ru&width=90&height=90&version=4&build=7",
                "text": "Touch-версия Почты стала еще удобнее",
                "action": "know-more",
                "date": "1467299722336",
                "title": "Узнать больше",
                "href": "https://e.mail.ru"
            },
            {
                "img": "https://filin.mail.ru/pic?email=n.panferov@corp.mail.ru&width=90&height=90&version=4&build=7",
                "text": "Touch-версия Почты стала еще удобнее",
                "action": "touch-mail",
                "date": "1467299722336"
            },
            {
                "img": "https://filin.mail.ru/pic?email=sumin@corp.mail.ru&width=90&height=90&version=4&build=7",
                "text": "Расширенный поиск и отмена действий в Почте для iOS",
                "date": "1473782919602"
            }
        ]
    };
});
//# sourceMappingURL=news-widget-transport.mocks.js.map